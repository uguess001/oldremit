$(document).ready(function() {
    $('.side-nav a').click(function() {
        $('.side-nav a').removeClass('active');
        $(this).addClass('active');
        var tagid = $(this).data('tag');
        $('.classinfo-abt').removeClass('active').addClass('hide');
        $('#' + tagid).addClass('active').removeClass('hide');
    });
    $('.cin-item').click(function() {
        $('.cin-item').removeClass('active');
        $(this).addClass('active');
        var tagid = $(this).data('tag');
        $('.card-item').removeClass('active').addClass('hide');
        $('#' + tagid).addClass('active').removeClass('hide');
    });
    $('#comp-pro').click(function() {
        $('.main-my-details').addClass('hide');
        $('.my-profile-complet').addClass('active');

    });
    $('#comp-pro-back').click(function() {
        $('.main-my-details').toggleClass('hide');
        $('.my-profile-complet').toggleClass('hide').removeClass('active');

    });
    $('.add-rec').click(function() {
        $('.list-info-rep').addClass('hide');
        $('.add-info-rep').addClass('active');

    });
   
    

});
$('#has_promo_code').change(function(){
    // if($(this).is(":checked")) {
        $('.promo-s').toggleClass('active');
    // } else {
        // $('.promo-s').removeClass('red');
    // }
});

  
  onload = function() {
    document.getElementsByClassName('cardnumber').oninput = function() {
      this.value = cc_format(this.value)
    }
  }
  function cc_format(value) {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    var matches = v.match(/\d{4,16}/g);
    var match = matches && matches[0] || ''
    var parts = []
    for (i=0, len=match.length; i<len; i+=4) {
      parts.push(match.substring(i, i+4))
    }
    if (parts.length) {
      return parts.join(' ')
    } else {
      return value
    }
  }